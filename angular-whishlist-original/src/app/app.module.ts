import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { StoreModule as NgRxStoreModule, ActionReducerMap} from '@ngrx/store';
import { EffectsModule} from '@ngrx/effects';
import { AppComponent } from './app.component';
import { AngularDestinoViajeComponent } from './angular-destino-viaje/angular-destino-viaje.component';
import { AngularListaDestinosComponent } from './angular-lista-destinos/angular-lista-destinos.component';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';
import { DestinosApiClient } from './models/destinos-api-client.model';
import { FormDestinoViajeComponent } from './form-destino-viaje/form-destino-viaje.component';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { initializeDestinosViajesState, reducerDestinosViajes, DestinosViajesState, DestinosViajesEffects } from './models/destinos-viajes-state.model';

const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: AngularListaDestinosComponent },
    { path: 'destino/:id', component: DestinoDetalleComponent }
  ];

// redux init
export interface AppState {
  destinos: DestinosViajesState;
}

const reducers: ActionReducerMap<AppState> = {
  destinos: reducerDestinosViajes
};

let reducersInitialState = {
  destinos: initializeDestinosViajesState()
}
// redux fin init

@NgModule({
  declarations: [
    AppComponent,
    AngularDestinoViajeComponent,
    AngularListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    NgRxStoreModule,
  ],

  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    NgRxStoreModule.forRoot(reducers, {initialState: reducersInitialState}),
    EffectsModule.forRoot([DestinosViajesEffects])
    StoreDevtoolsModule.instrument(),
  ],

  providers: [
    DestinosApiClient
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}