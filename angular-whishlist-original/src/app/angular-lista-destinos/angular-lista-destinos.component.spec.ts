import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AngularListaDestinosComponent } from './angular-lista-destinos.component';

describe('AngularListaDestinosComponent', () => {
  let component: AngularListaDestinosComponent;
  let fixture: ComponentFixture<AngularListaDestinosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AngularListaDestinosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AngularListaDestinosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
