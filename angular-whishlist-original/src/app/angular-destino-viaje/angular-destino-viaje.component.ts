import { Component, OnInit, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import { DestinoViaje} from './../models/destino-viaje.model';
import { Store} from '@ngrx/store';
import { AppState} from '../app.module';
import { VoteDownAction, VoteUpAction} from './../models/destinos-viajes-state.model';

@Component({
  selector: 'app-angular-destino-viaje',
  templateUrl: './angular-destino-viaje.component.html',
  styleUrls: ['./angular-destino-viaje.component.css']
})
export class AngularDestinoViajeComponent implements OnInit {
  @Input() destino: DestinoViaje;
  @Input("idx") position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() onclicked: EventEmitter<DestinoViaje>;

  constructor(private store: Store <AppState>) { 
    this.onclicked = new EventEmitter();
  }

  ngOnInit(): void {
  }

  Ir() {
    this.onclicked.emit(this.destino);
    return false;
  }
  
  voteUp() {
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }

  voteDown() {
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }
}
