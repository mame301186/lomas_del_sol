import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AngularDestinoViajeComponent } from './angular-destino-viaje.component';

describe('AngularDestinoViajeComponent', () => {
  let component: AngularDestinoViajeComponent;
  let fixture: ComponentFixture<AngularDestinoViajeComponent>;

  beforeEach(async (() => {
    TestBed.configureTestingModule({
      declarations: [ AngularDestinoViajeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AngularDestinoViajeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
