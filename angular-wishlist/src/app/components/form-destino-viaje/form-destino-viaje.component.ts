import { Component, OnInit, Output, EventEmitter, Inject, forwardRef } from '@angular/core';
import { DestinosViajes } from '../../models/destino-viajes.model';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { fromEvent } from 'rxjs';
import {
  map,
  filter,
  debounceTime,
  distinctUntilChanged,
  switchMap,
} from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { AppConfig, APP_CONFIG } from 'src/app/app.module';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css'],
})
export class FormDestinosViajesComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinosViajes>;
  fg: FormGroup;
  minLongitud = 3;
  searchResults: string[];

  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) {
    this.onItemAdded = new EventEmitter();
    this.crearformulario();
  }

  ngOnInit(): void {
    this.resultadoBusqueda();
  }

  resultadoBusqueda() {
    const elemenNombre = <HTMLInputElement>document.querySelector('#nombre');
    fromEvent(elemenNombre, 'input') // genera elementos observable de entrada
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value), // captura doda la palabra hasta el momento
        filter((text) => text.length > 4), //Sigue con la tarea si se cumple
        debounceTime(120), // trae todo lo0 que este asta los 200
        distinctUntilChanged(),
        switchMap((text: string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text))
      ).subscribe(AjaxResponse => this.searchResults = AjaxResponse.response);
  }

  crearformulario() {
    this.fg = this.fb.group({
      nombre: [
        '',
        Validators.compose([
          Validators.required,
          this.nombreValidator,
          this.nombreValidatorPararametrizable(this.minLongitud),
        ]),
      ],
      url: [
        '',
        Validators.compose([
          Validators.required,
          this.nombreValidator,
          this.nombreValidatorPararametrizable(this.minLongitud),
        ]),
      ],
    });
    this.fg.valueChanges.subscribe((form: any) => {
      // console.log(`El cambio de formulario: ${form}`);
    });
  }

  guardar() {
    const d = new DestinosViajes(
      this.fg.get('nombre').value,
      this.fg.get('url').value
    );
    this.onItemAdded.emit(d);
  }

  nombreValidator(control: FormControl): { [s: string]: boolean } {
    let l = control.value.toString().trim().length;
    return l > 0 && l < 5 ? { invalidNombre: true } : null;
  }

  nombreValidatorPararametrizable(minLong: number) {
    return (control: FormControl): { [s: string]: boolean } | null => {
      let l = control.value.toString().trim().length;
      return l > 0 && l < minLong ? { minLogNombre: true } : null;
    };
  }
}
