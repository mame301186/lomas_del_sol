import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { DestinosViajes } from '../../models/destino-viajes.model';
import { AppState } from '../../app.module';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient],
})

export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinosViajes>;
  updates: string[];
  all;

  constructor(
    private destinosApiClient: DestinosApiClient,
    private store: Store<AppState>
  ) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
  }

  ngOnInit(): void {
    this.itemFavoritos();
  }

  itemFavoritos() {
    this.store
      .select((state) => state.destinos.favorito) // solo la actualizacione sde destinos
      .subscribe((d) => {
        if (d != null) {
          this.updates.push(`Se ha elegido a ${d.nombre}`);
        }
      });
    this.store
      .select((state) => state.destinos.items)
      .subscribe((items) => (this.all = items));
  }

  agregado(d: DestinosViajes) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  elegido(e: DestinosViajes) {
    this.destinosApiClient.elegir(e);
  }
}
